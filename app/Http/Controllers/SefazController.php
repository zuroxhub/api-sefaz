<?php

namespace App\Http\Controllers;

use App\Helpers\Fiscal;
use Illuminate\Http\Request;
use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;

class SefazController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index() {
        return [
            'message'=>'sefaz-controller'
        ];
    }

    public function getEmpresaCnpj( $uf, $cnpj , Request $request){




        try {


           # if($request->session()->has('cnpj'.$cnpj) ){

              #  $response = $request->session()->get('cnpj'.$cnpj);
             #   return $response;

            #}else{


                $Fiscal = new Fiscal($request);
                $simples = $Fiscal->getCnpjDetails( $cnpj );

                dd($simples);
                if( $simples != 'unavaliable'){
                   # $request->session()->put('cnpj'.$cnpj,$simples);
                  #  $request->session()->save();
                }else{

                }

                $request->session()->put('cnpj'.$cnpj,$simples);
                $request->session()->save();

           # }

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
/*
        $config = [
            "tpAmb" => 1,
            "razaosocial"=>"",
            "schemes" => "PL_008i2",
            "versao" => "3.10",
            "siglaUF" => "SP",
            "cnpj" =>  '24985952000170',

        ];


        $configJson = json_encode($config);
        try{


            try {

                $certificate = Certificate::readPfx(file_get_contents( base_path('.knowa/zurox.pfx') ), 12345678);
                $tools = new Tools($configJson, $certificate);
                $tools->model('55');

                $uf = 'SP';
                $cnpj = '61585865073701';
                $iest = '';
                $cpf = '';
                $response = $tools->sefazCadastro($uf, $cnpj, $iest, $cpf);

                header('Content-type: text/json; charset=UTF-8');
               # echo $response;

                //você pode padronizar os dados de retorno atraves da classe abaixo
                //de forma a facilitar a extração dos dados do XML
                //NOTA: mas lembre-se que esse XML muitas vezes será necessário,
                //      quando houver a necessidade de protocolos
                $stdCl = new Standardize($response);
                //nesse caso $std irá conter uma representação em stdClass do XML
               # $std = $stdCl->toStd();
                //nesse caso o $arr irá conter uma representação em array do XML
                #$arr = $stdCl->toArray();
                //nesse caso o $json irá conter uma representação em JSON do XML
                $json = $stdCl->toJson();

                return $json;

            } catch (\Exception $e) {
                echo $e->getMessage();
            }
/*

            $configJson = json_encode($config);
            dd($configJson);
            $certificate = Certificate::readPfx(file_get_contents( base_path('.knowa/zurox.pfx') ), 12345678);

            $tools = new Tools($configJson, $certificate);
            $tools->model('55');
            $response = $tools->sefazCadastro($uf, $cnpj, $iest = '', $cpf = '');
            $stdCl = new Standardize($response);

            $Data = $stdCl->toArray();

            if(isset($Data['infCons']['xMotivo']) && !isset($Data['infCons']['infCad'])){
                return $Data['infCons']['xMotivo'];
            }

            return $Data['infCons']['infCad'];* /

        }catch ( \Exception $e){
            return [
                'errors'=>$e->getMessage()
            ];
        }*/

       # Session::put('sefaz'.$cnpj,json_encode($Data['infCons']['infCad']));
       # Session::save();

       # return json_decode(Session::get('sefaz'.$cnpj));


    }


    public function getCnpjSefaz( $uf, $cnpj , $isCommand = false){

        if( Cache::has('sefaz'.$cnpj)){
            #return json_decode(Cache::get('sefaz'.$cnpj));
        }

        $config = [
            "tpAmb" => 1,
            "razaosocial"=>"",
            "schemes" => "PL_008i2",
            "versao" => "3.10",
            "siglaUF" => "SP",
            "cnpj" =>  env('ENOTASGW_FRANK_NUMBER'),

        ];


        if($isCommand){

            #dd( $_SERVER['APP_CLOUD_PATH'] );
            #dd(file_get_contents( $_SERVER['APP_CLOUD_PATH'].'/public/'.env('ENOTASGW_FRANK') ));
            # dd($_SERVER['APP_CLOUD_PATH']);
            #$certificate = Certificate::readPfx(file_get_contents( 'public/'.env('ENOTASGW_FRANK') ), env('ENOTASGW_FRANK_PASSWORD'));
            # $certificate = Certificate::readPfx(file_get_contents('/public/.knowa/zurox.pfx' ), env('ENOTASGW_FRANK_PASSWORD'));
            $config['cnpj']='24985952000170';
            $configJson = json_encode($config);
            $certificate = Certificate::readPfx(file_get_contents('./public/.knowa/zurox.pfx' ), 12345678);
            #$certificate = Certificate::readPfx(file_get_contents(env('APP_CLOUD_PATH').'/public/.knowa/zurox.pfx' ), 12345678);
        }else{
            $configJson = json_encode($config);
            $certificate = Certificate::readPfx(file_get_contents( env('ENOTASGW_FRANK') ), env('ENOTASGW_FRANK_PASSWORD'));
        }

        $tools = new Tools($configJson, $certificate);
        $tools->model('55');

        $iest = '';
        $cpf = '';
        $response = $tools->sefazCadastro($uf, $cnpj, $iest, $cpf);
        $stdCl = new Standardize($response);

        $Data = $stdCl->toArray();

        if(isset($Data['infCons']['xMotivo'])){
            #return $Data['infCons']['xMotivo'];
        }

        if(isset($Data['infCons']['xMotivo']) && !isset($Data['infCons']['infCad'])){
            return $Data['infCons']['xMotivo'];
        }

        dd('FINAL');

       # Cache::put('sefaz'.$cnpj,json_encode($Data['infCons']['infCad']),30);

        #print_r($Data);
        #return $Data['infCons']['infCad'];

       # return json_decode(Cache::get('sefaz'.$cnpj));


    }

}
