<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Session;

class Fiscal {

    protected $req;

    public function __construct($request)
    {
        $this->req = $request;
    }

    public function getCnpjDetails( $cnpj ) {

            $response = Curl::to('https://www.receitaws.com.br/v1/cnpj/'.$cnpj)
                ->withContentType('application/json')
                ->withHeaders([
                    "Content-Type: application/json",
                    "Accept: application/json",
                    "Origin: http//www.zhub2.com.br/"
                ])
                ->asJson()
                ->get();

            if( isset($response->nome) ){
                return $response;
            }else{
                return 'unavaliable';
            }


        /*
          $response = Curl::to('https://www.receitaws.com.br/v1/cnpj/'.$cnpj)
              ->withContentType('application/json')
              ->withHeaders([
                  "Content-Type: application/json",
                  "Accept: application/json",
                  "Origin: http//www.zhub2.com.br/"
              ])
              ->asJson()
              ->get();
          return $response;
            */
    }


}